module org.openjfx {
    requires javafx.controls;
    requires org.slf4j;
    exports org.openjfx;
}