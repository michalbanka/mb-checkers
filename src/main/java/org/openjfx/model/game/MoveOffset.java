package org.openjfx.model.game;

import java.util.Objects;

public class MoveOffset {

    private final int columnOffset;

    private final int rowOffset;

    public MoveOffset(int columnOffset, int rowOffset) {
        this.columnOffset = columnOffset;
        this.rowOffset = rowOffset;
    }

    public int getColumnOffset() {
        return columnOffset;
    }

    public int getRowOffset() {
        return rowOffset;
    }

    public MoveOffset getOpposedOffset() {
        return new MoveOffset(-columnOffset, -rowOffset);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoveOffset that = (MoveOffset) o;
        return columnOffset == that.columnOffset && rowOffset == that.rowOffset;
    }

    @Override
    public int hashCode() {
        return Objects.hash(columnOffset, rowOffset);
    }
}
