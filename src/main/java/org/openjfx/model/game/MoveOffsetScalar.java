package org.openjfx.model.game;

public enum MoveOffsetScalar {
    UP_LEFT(-1, -1),
    UP_RIGHT(1, -1),
    DOWN_LEFT(-1, 1),
    DOWN_RIGHT(1, 1);

    private final MoveOffset moveOffset;

    MoveOffsetScalar(int columnOffset, int rowOffset) {
        this.moveOffset = new MoveOffset(columnOffset, rowOffset);
    }

    public MoveOffset getMoveOffset() {
        return moveOffset;
    }
}
