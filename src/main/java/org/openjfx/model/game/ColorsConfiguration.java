package org.openjfx.model.game;

import javafx.scene.paint.Color;

public enum ColorsConfiguration {

    BLACK_PLAYER(Color.gray(0.1)),
    WHITE_PLAYER(Color.FLORALWHITE.brighter()),
    STUB_PLAYER(new Color(1, 0, 0, 0.3)),

    PIECE_HINT(Color.YELLOWGREEN),
    BOARD_GRID_HINT(PIECE_HINT.color),
    BOARD_GRID_HINT_LAST(PIECE_HINT.color.darker()),

    BACKGROUND(Color.gray(0.25)),

    DARK_FIELD_COLOR(Color.DIMGRAY.brighter()),
    LIGHT_FIELD_COLOR(DARK_FIELD_COLOR.getColor().brighter());

    private final Color color;

    ColorsConfiguration(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
