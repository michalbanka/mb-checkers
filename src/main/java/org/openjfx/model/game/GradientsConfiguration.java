package org.openjfx.model.game;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

public enum GradientsConfiguration {

    GOLDEN_GRADIENT_DENSE(new LinearGradient(20,20,80, 80, false, CycleMethod.REFLECT, new Stop(0, Color.GOLDENROD), new Stop(100, Color.GOLD))),
    GOLDEN_GRADIENT(new LinearGradient(20,20,50, 50, false, CycleMethod.REFLECT, new Stop(0, Color.GOLDENROD), new Stop(50, Color.GOLD)));

    private final LinearGradient gradient;

    GradientsConfiguration(LinearGradient gradient) {
        this.gradient = gradient;
    }

    public LinearGradient getGradient() {
        return gradient;
    }
}
