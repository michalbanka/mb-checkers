package org.openjfx.model.game;

import javafx.scene.paint.Color;

public enum Player {
    BLACK(ColorsConfiguration.BLACK_PLAYER.getColor()),
    WHITE(ColorsConfiguration.WHITE_PLAYER.getColor()),
    NONE(ColorsConfiguration.STUB_PLAYER.getColor());

    private final Color color;

    Player(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

}
