package org.openjfx.model.game;

public enum MoveType {
    MOVE(1),
    CAPTURE(2);

    private final int stepMultiplier;

    MoveType(int stepMultiplier) {
        this.stepMultiplier = stepMultiplier;
    }

    public int getStepMultiplier() {
        return stepMultiplier;
    }
}
