package org.openjfx.model.game;

import java.util.List;

public enum ValidPlayerMoveDirections {
    POSSIBLE_WHITE_PIECE_MOVE_DIRECTIONS(
            MoveOffsetScalar.UP_RIGHT.getMoveOffset(),
            MoveOffsetScalar.UP_LEFT.getMoveOffset()
    ),
    POSSIBLE_BLACK_PIECE_MOVE_DIRECTIONS(
            MoveOffsetScalar.DOWN_RIGHT.getMoveOffset(),
            MoveOffsetScalar.DOWN_LEFT.getMoveOffset()
    ),
    POSSIBLE_CAPTURE_ALL_DIRECTIONS(
            MoveOffsetScalar.UP_RIGHT.getMoveOffset(),
            MoveOffsetScalar.UP_LEFT.getMoveOffset(),
            MoveOffsetScalar.DOWN_RIGHT.getMoveOffset(),
            MoveOffsetScalar.DOWN_LEFT.getMoveOffset());

    private final List<MoveOffset> moveOffsets;

    ValidPlayerMoveDirections(MoveOffset... moveOffsets) {
        this.moveOffsets = List.of(moveOffsets);
    }

    public List<MoveOffset> getMoveOffsets() {
        return moveOffsets;
    }
}
