package org.openjfx.model.game;

import javafx.scene.Node;
import org.openjfx.gui.game.board.BoardPanel;
import org.openjfx.gui.game.board.Grid;
import org.openjfx.model.game.Position;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Board {

    private final List<List<Grid>> board;

    public Board(List<Node> board) {
        this.board = IntStream.range(0, BoardPanel.GRIDS)
                    .boxed()
                    .map(row -> getRow(board, row))
                    .collect(Collectors.toList());
    }

    public Grid getGrid(Position position) {
        return board.get(position.getRow()).get(position.getColumn());
    }

    public List<List<Grid>> getBoard() {
        return this.board;
    };

    private List<Grid> getRow(List<Node> board, int row) {
        int fromIndex = row * BoardPanel.GRIDS;
        int toIndex = (row + 1) * BoardPanel.GRIDS;
        return board.subList(fromIndex, toIndex).stream()
                .map(node -> (Grid) node)
                .collect(Collectors.toList());
    }
}
