package org.openjfx.model.game;

import java.util.Collections;
import java.util.List;

public class Move {
    private final Position position;
    private List<Move> nextMoves;

    public Move(Position position, List<Move> nextMoves) {
        this.position = position;
        this.nextMoves = nextMoves;
    }

    public Position getPosition() {
        return position;
    }

    public List<Move> getNextMoves() {
        return nextMoves;
    }

    public boolean doesContainPosition(Position p) {
        if (position.equals(p)) {
            return true;
        }
        return nextMoves.stream()
                .map(move -> move.doesContainPosition(p))
                .anyMatch(aBoolean -> true);
    }

    public Move truncateTo(Position position) {
        if (this.position.equals(position)) {
            this.nextMoves = Collections.emptyList();
        }
        if (nextMoves != null) {
            nextMoves.forEach(move -> move.truncateTo(position));
        }
        return this;
    }

    @Override
    public String toString() {
        return "Move{" +
                "position=" + position +
                ", nextMove=" + nextMoves +
                '}';
    }
}
