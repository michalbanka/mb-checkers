package org.openjfx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.openjfx.gui.menu.MenuPanel;

public class App extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) {
        var menuPanel = new MenuPanel();
        var scene = new Scene(menuPanel, 640, 480);
        stage.setScene(scene);
        stage.setTitle("MB Checkers");
        stage.setResizable(false);
        stage.show();
    }

}