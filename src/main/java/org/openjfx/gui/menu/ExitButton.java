package org.openjfx.gui.menu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExitButton extends MenuButton {
    private static final Logger log = LoggerFactory.getLogger(ExitButton.class);
    private static final String EXIT = "EXIT";

    private final EventHandler<ActionEvent> eventHandler = actionEvent -> {
        log.info("Application closing...");
        System.exit(0);
    };

    public ExitButton() {
        super();
        this.setOnAction(eventHandler);
        this.setText(EXIT);
    }

}
