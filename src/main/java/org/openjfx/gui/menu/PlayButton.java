package org.openjfx.gui.menu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.openjfx.gui.game.GamePanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlayButton extends MenuButton {
    private static final Logger log = LoggerFactory.getLogger(PlayButton.class);
    private static final String PLAY = "PLAY";

    public PlayButton() {
        super();
        this.setOnAction(getEventHandler());
        this.setText(PLAY);
    }

    private EventHandler<ActionEvent> getEventHandler() {
        return actionEvent -> {
            log.info("Opening game window");
            Stage stage = (Stage) this.getScene().getWindow();
            GamePanel gamePanel = new GamePanel();
            Scene scene = new Scene(gamePanel);
            stage.setScene(scene);
        };
    }
}
