package org.openjfx.gui.menu;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import org.openjfx.model.game.ColorsConfiguration;

import java.util.List;

public class MenuPanel extends VBox {

    public MenuPanel() {
        super();
        this.getChildren().addAll(getMenuButtons());
        this.setAlignment(Pos.CENTER);
        this.setSpacing(10);
        this.setBackground(new Background(new BackgroundFill(ColorsConfiguration.BACKGROUND.getColor(), CornerRadii.EMPTY, Insets.EMPTY)));
    }

    private List<Button> getMenuButtons() {
        PlayButton playButton = new PlayButton();
        ExitButton exitButton = new ExitButton();
        return List.of(playButton, exitButton);
    }

}
