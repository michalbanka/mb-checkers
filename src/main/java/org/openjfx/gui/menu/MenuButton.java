package org.openjfx.gui.menu;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.text.Font;
import org.openjfx.model.game.ColorsConfiguration;
import org.openjfx.model.game.GradientsConfiguration;

public class MenuButton extends Button {

    public MenuButton() {
        super();
        this.setMinSize(200,50);
        this.setFont(Font.font(28));
        this.setBorder(new Border(new BorderStroke(ColorsConfiguration.BACKGROUND.getColor().darker(), BorderStrokeStyle.SOLID, new CornerRadii(10),BorderStroke.MEDIUM)));
        this.setBackground(new Background(new BackgroundFill(GradientsConfiguration.GOLDEN_GRADIENT_DENSE.getGradient(), new CornerRadii(13), Insets.EMPTY)));
    }
}
