package org.openjfx.gui.game.information;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class InfoText extends Text {

    public InfoText(String text) {
        super(text);
        this.setFont(Font.font(20));
        this.setFill(Color.LIGHTGREY);
    }
}
