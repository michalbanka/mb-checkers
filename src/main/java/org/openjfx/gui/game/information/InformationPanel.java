package org.openjfx.gui.game.information;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import org.openjfx.model.game.Player;
import org.openjfx.state.PlayerMoveState;

public class InformationPanel extends VBox {

    private final CurrentMoveInfo currentMoveInfo;

    public InformationPanel() {
        super();
        this.setPadding(new Insets(10));
        this.setAlignment(Pos.CENTER);

        this.currentMoveInfo = new CurrentMoveInfo();
        this.getChildren().add(currentMoveInfo);

        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(500), event -> updatePanel()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private void updatePanel() {
        this.currentMoveInfo.update();
    }
}
