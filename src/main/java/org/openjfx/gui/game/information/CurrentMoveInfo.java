package org.openjfx.gui.game.information;

import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.openjfx.state.PlayerMoveState;

public class CurrentMoveInfo extends GridPane {

    private final Rectangle rectangle;

    public CurrentMoveInfo() {
        InfoText text = new InfoText("Current player move: ");
        rectangle = new Rectangle(35, 35, PlayerMoveState.getActualPlayer().getColor());
        this.add(text, 0, 0);
        this.add(rectangle, 1, 0);
        this.setAlignment(Pos.CENTER);
        rectangle.setArcHeight(10);
        rectangle.setArcWidth(10);
    }

    public void update() {
        Color color = PlayerMoveState.getActualPlayer().getColor();
        rectangle.setFill(color);
    }
}