package org.openjfx.gui.game;

import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import org.openjfx.gui.game.board.BoardPanel;
import org.openjfx.gui.game.information.InformationPanel;
import org.openjfx.gui.game.timer.TimerPanel;
import org.openjfx.model.game.ColorsConfiguration;

public class GamePanel extends BorderPane {

    public GamePanel() {
        BoardPanel boardPanel = new BoardPanel();
        TimerPanel timerPanel = new TimerPanel();
        InformationPanel informationPanel = new InformationPanel();
        this.setTop(timerPanel);
        this.setCenter(boardPanel);
        this.setBottom(informationPanel);
        this.setBackground(new Background(new BackgroundFill(ColorsConfiguration.BACKGROUND.getColor(), CornerRadii.EMPTY, Insets.EMPTY)));
    }
}
