package org.openjfx.gui.game.timer;

import javafx.scene.paint.Color;
import org.openjfx.gui.game.information.InfoText;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;

public class TimerText extends InfoText {

    private final LocalTime startTime;

    public TimerText(LocalTime time) {
        super("00:00");
        this.setFill(Color.DARKGREY);
        this.startTime = time;
    }

    public void update(){
        long secondsPassed = LocalTime.now().toEpochSecond(LocalDate.MIN, ZoneOffset.UTC) - startTime.toEpochSecond(LocalDate.MIN, ZoneOffset.UTC);
        long minutes = secondsPassed / 60;
        long seconds = secondsPassed % 60;
        String timerText = String.format("%02d:%02d", minutes, seconds);
        this.setText(timerText);
    }

}
