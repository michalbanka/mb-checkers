package org.openjfx.gui.game.timer;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import org.openjfx.gui.game.information.InfoText;
import org.openjfx.state.PlayerMoveState;

import java.time.LocalTime;

public class TimerPanel extends HBox {

    private final TimerText timer;

    public TimerPanel() {
        super();
        this.setPadding(new Insets(10, 0, 0, 0));
        this.setAlignment(Pos.TOP_CENTER);
        this.timer = new TimerText(LocalTime.now());
        this.getChildren().add(timer);

        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(1000), event -> updatePanel()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private void updatePanel() {
        this.timer.update();
    }
}
