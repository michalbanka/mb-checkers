package org.openjfx.gui.game.board;

import javafx.scene.layout.StackPane;
import org.openjfx.model.game.Position;

import java.util.Optional;

public class Grid extends StackPane {

    public static final double SIZE_PX = 70;

    private final Position position;

    Grid(Position position) {
        super();
        this.position = position;
    }

    public Optional<Piece> getPiece() {
        try {
            return Optional.of((Piece) this.getChildren().get(1));
        } catch (IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

    public BoardGrid getBoardGrid() {
        return (BoardGrid) this.getChildren().get(0);
    }

    public Position getPosition() {
        return position;
    }

    public boolean addPiece(Piece piece) {
        if (this.getChildren().size() == 1) {
            return this.getChildren().add(piece);
        } else {
            return false;
        }
    }

    public boolean removePiece() {
        if (this.getChildren().size() == 2) {
            return this.getChildren().remove(1) != null;
        } else {
            return false;
        }
    }


}



