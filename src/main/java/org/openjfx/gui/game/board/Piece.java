package org.openjfx.gui.game.board;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.openjfx.model.game.Player;

public class Piece extends Circle {

    private static final double SIZE_FACTOR = 0.4d;

    private final Player player;

    public Piece(Player player) {
        super();
        this.player = player;
        this.setRadius(Grid.SIZE_PX * SIZE_FACTOR);
        this.setFill(player.getColor());
    }

    public Color getColor() {
        return player.getColor();
    }

    public Player getPlayer() {
        return this.player;
    }

}
