package org.openjfx.gui.game.board;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.GridPane;
import org.openjfx.events.GridClickEventHandler;
import org.openjfx.events.GridEvent;
import org.openjfx.model.game.Board;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BoardPanel extends GridPane {

    public static final int GRIDS = 8;

    public BoardPanel() {
        super();
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(10, 25, 10, 25));
        this.setGridLinesVisible(true);
        generateGrids();
        this.addEventHandler(GridEvent.GRID_CLICKED, new GridClickEventHandler(getBoard()));
    }

    private void generateGrids() {
        IntStream.range(0, GRIDS)
                .boxed()
                .forEach(row -> this.addRow(row, generateRow(row).toArray(Node[]::new)));
    }

    private List<Grid> generateRow(int row) {
        return IntStream.range(0, GRIDS)
                .boxed()
                .map(column -> new GridFactory().getGrid(column, row))
                .collect(Collectors.toList());
    }

    private Board getBoard() {
        ObservableList<Node> children = getChildren();
        return new Board(children.subList(1, children.size()));
    }

}
