package org.openjfx.gui.game.board;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.openjfx.model.game.ColorsConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BoardGrid extends Rectangle {

    public static final Color LIGHT_FIELD_COLOR = ColorsConfiguration.LIGHT_FIELD_COLOR.getColor();
    public static final Color DARK_FIELD_COLOR = ColorsConfiguration.DARK_FIELD_COLOR.getColor();

    private static final Logger log = LoggerFactory.getLogger(BoardGrid.class);
    private final Color color;

    public BoardGrid(int column, int row) {
        super();
        this.color = getColor(column, row);

        this.setX(Grid.SIZE_PX * column);
        this.setY(Grid.SIZE_PX * row);
        this.setWidth(Grid.SIZE_PX);
        this.setHeight(Grid.SIZE_PX);
        this.setFill(color);
        this.setOnMouseClicked(mouseEvent -> log.debug("Grid: Column={}, Row={}", column, row));
    }

    public Color getColor() {
        return color;
    }

    private Color getColor(int column, int row) {
        if (isLightGrid(column, row)) {
            return LIGHT_FIELD_COLOR;
        } else {
            return DARK_FIELD_COLOR;
        }
    }

    private boolean isLightGrid(int column, int row) {
        return column % 2 == 0 && row % 2 == 0 || column % 2 == 1 && row % 2 == 1;
    }
}
