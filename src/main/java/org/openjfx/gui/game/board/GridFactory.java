package org.openjfx.gui.game.board;

import javafx.scene.paint.Color;
import org.openjfx.events.GridEvent;
import org.openjfx.model.game.Player;
import org.openjfx.model.game.Position;

public class GridFactory {

    public static final int STARTING_PIECE_ROWS = 3;

    public Grid getGrid(int column, int row) {
        Position position = new Position(column, row);
        Grid grid = new Grid(position);
        grid.setOnMouseClicked(e -> initiateGridClickedEvent(position, grid));

        BoardGrid boardGrid = new BoardGrid(column, row);
        grid.getChildren().add(boardGrid);
        placePiece(grid, boardGrid.getColor());

        return grid;
    }

    private void initiateGridClickedEvent(Position position, Grid grid) {
        grid.fireEvent(new GridEvent(GridEvent.GRID_CLICKED, position));
        grid.fireEvent(new GridEvent(GridEvent.TEST, position));
    }

    private void placePiece(Grid grid, Color boardColor) {
        if (isBlacksStartingGrid(grid.getPosition(), boardColor)) {
            Piece piece = new Piece(Player.BLACK);
            grid.getChildren().add(piece);
        } else if (isWhitesStartingGrid(grid.getPosition(), boardColor)) {
            Piece piece = new Piece(Player.WHITE);
            grid.getChildren().add(piece);
        }
    }

    private boolean isBlacksStartingGrid(Position position, Color boardColor) {
        return position.getRow() < STARTING_PIECE_ROWS
                && boardColor.equals(BoardGrid.DARK_FIELD_COLOR);
    }

    private boolean isWhitesStartingGrid(Position position, Color boardColor) {
        return position.getRow() >= BoardPanel.GRIDS - STARTING_PIECE_ROWS
                && boardColor.equals(BoardGrid.DARK_FIELD_COLOR);
    }
}
