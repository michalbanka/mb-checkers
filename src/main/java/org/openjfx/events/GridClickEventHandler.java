package org.openjfx.events;

import javafx.event.EventHandler;
import org.openjfx.gui.game.board.Grid;
import org.openjfx.model.game.Board;
import org.openjfx.model.game.Move;
import org.openjfx.model.game.Position;
import org.openjfx.service.game.BoardResetService;
import org.openjfx.service.game.GridColoringService;
import org.openjfx.service.game.moves.PieceMover;
import org.openjfx.service.game.moves.PossibleMovesAnalyzer;
import org.openjfx.state.PlayerMoveState;
import org.openjfx.state.PressedPieceState;

import java.util.List;

public class GridClickEventHandler implements EventHandler<GridEvent> {

    private final Board board;
    private final BoardResetService boardResetService;
    private final GridColoringService gridColoringService;
    private final PossibleMovesAnalyzer possibleMovesResolver;
    private final PieceMover pieceMover;

    public GridClickEventHandler(Board board) {
        this.board = board;
        this.boardResetService = new BoardResetService();
        this.gridColoringService = new GridColoringService();
        this.possibleMovesResolver = new PossibleMovesAnalyzer();
        this.pieceMover = new PieceMover();
    }

    @Override
    public void handle(GridEvent gridEvent) {
        Position clickedPosition = gridEvent.getPosition();
        boardResetService.reset(board);

        if (shouldShowHint(board, clickedPosition)) {
            gridColoringService.showMoveHints(board, clickedPosition);
            PressedPieceState.POSITION = clickedPosition;
        } else if (shouldMove(board, clickedPosition)) {
            Move move = getMove(board, clickedPosition);
            pieceMover.move(board.getGrid(PressedPieceState.POSITION), board.getGrid(clickedPosition));
            pieceMover.remove(board, PressedPieceState.POSITION, move);
            PressedPieceState.POSITION = null;
            PlayerMoveState.nextMove();
        }
    }

    private boolean shouldShowHint(Board board, Position clickedPosition) {
        Grid grid = board.getGrid(clickedPosition);
        return grid.getPiece().isPresent()
                && grid.getPiece().get().getPlayer().equals(PlayerMoveState.getActualPlayer());
    }

    private boolean shouldMove(Board board, Position clickedPosition) {
        return PressedPieceState.POSITION != null
                && board.getGrid(PressedPieceState.POSITION).getPiece().isPresent()
                && board.getGrid(PressedPieceState.POSITION).getPiece().get().getPlayer().equals(PlayerMoveState.getActualPlayer())
                && isValidMove(board, clickedPosition);
    }

    private boolean isValidMove(Board board, Position clickedPosition) {
        List<Move> moves = possibleMovesResolver.resolve(board, PressedPieceState.POSITION);
        return moves.stream()
                .anyMatch(move -> move.doesContainPosition(clickedPosition));
    }

    private Move getMove(Board board, Position clickedPosition) {
        return possibleMovesResolver.resolve(board, PressedPieceState.POSITION).stream()
                .filter(move -> move.doesContainPosition(clickedPosition))
                .findFirst()
                .map(move -> move.truncateTo(clickedPosition))
                .orElseThrow();
    }
}
