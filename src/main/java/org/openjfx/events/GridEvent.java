package org.openjfx.events;

import javafx.event.Event;
import javafx.event.EventType;
import org.openjfx.model.game.Position;

public class GridEvent extends Event {

    public static final EventType<GridEvent> GRID_CLICKED = new EventType<>(Event.ANY, "GRID_CLICKED");
    public static final EventType<GridEvent> TEST = new EventType<>(Event.ANY, "TEST");
    private final Position position;

    public GridEvent(EventType<? extends Event> eventType, Position position) {
        super(eventType);
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }
}
