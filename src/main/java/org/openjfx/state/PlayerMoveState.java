package org.openjfx.state;

import org.openjfx.model.game.Player;

public class PlayerMoveState {

    private static Player actualPlayer = Player.WHITE;

    public PlayerMoveState() {}

    public static Player getActualPlayer() {
        return actualPlayer;
    }

    public static void nextMove() {
        switch (actualPlayer) {
            case WHITE:
                actualPlayer = Player.BLACK;
                break;
            case BLACK:
                actualPlayer = Player.WHITE;
                break;
            default:
                actualPlayer = Player.NONE;
        }
    }
}
