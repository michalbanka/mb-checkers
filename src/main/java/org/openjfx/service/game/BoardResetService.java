package org.openjfx.service.game;

import org.openjfx.model.game.Board;
import org.openjfx.gui.game.board.BoardGrid;
import org.openjfx.gui.game.board.Grid;
import org.openjfx.gui.game.board.Piece;

import java.util.List;

public class BoardResetService {

    public BoardResetService() {}

    public void reset(Board board) {
        board.getBoard().forEach(this::resetRow);
    }

    private void resetRow(List<Grid> row) {
        row.forEach(grid -> {
            resetBoardGrid(grid.getBoardGrid());
            grid.getPiece().ifPresent(this::resetPiece);
        });
    }

    private void resetBoardGrid(BoardGrid boardGrid) {
        boardGrid.setFill(boardGrid.getColor());
    }

    private void resetPiece(Piece piece) {
        piece.setFill(piece.getColor());
    }
}
