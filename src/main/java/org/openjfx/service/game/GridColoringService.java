package org.openjfx.service.game;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;
import org.openjfx.model.game.Board;
import org.openjfx.model.game.GradientsConfiguration;
import org.openjfx.model.game.Move;
import org.openjfx.model.game.Position;
import org.openjfx.service.game.moves.PossibleMovesAnalyzer;

import java.util.List;

public class GridColoringService {

    private static final Paint PIECE_HINT_COLOR = GradientsConfiguration.GOLDEN_GRADIENT.getGradient();
    private static final Paint BOARD_GRID_HINT_COLOR = GradientsConfiguration.GOLDEN_GRADIENT_DENSE.getGradient();
    private static final Paint BOARD_GRID_CAPTURE_COLOR = GradientsConfiguration.GOLDEN_GRADIENT_DENSE.getGradient();

    private final PossibleMovesAnalyzer possibleMovesResolver;

    public GridColoringService() {
        this.possibleMovesResolver = new PossibleMovesAnalyzer();
    }

    public void showMoveHints(Board board, Position position) {
        List<Move> possibleMoves = possibleMovesResolver.resolve(board, position);
        colorPiece(board, position);
        colorBoardGrids(board, possibleMoves);
    }

    private void colorPiece(Board board, Position position) {
        board.getGrid(position).getPiece().ifPresent(piece -> piece.setFill(PIECE_HINT_COLOR));
    }

    private void colorBoardGrids(Board board, List<Move> possibleMoves) {
        possibleMoves.forEach(move -> {
            if (!move.getNextMoves().isEmpty()) {
                colorBoardGrids(board, move.getNextMoves());
            } else {
                board.getGrid(move.getPosition()).getBoardGrid().setFill(BOARD_GRID_CAPTURE_COLOR);
                return;
            }
            board.getGrid(move.getPosition()).getBoardGrid().setFill(BOARD_GRID_HINT_COLOR);
        });
    }
}
