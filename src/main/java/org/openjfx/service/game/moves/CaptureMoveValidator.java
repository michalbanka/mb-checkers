package org.openjfx.service.game.moves;

import org.openjfx.model.game.Board;
import org.openjfx.gui.game.board.Piece;
import org.openjfx.model.game.MoveOffset;
import org.openjfx.model.game.MoveType;
import org.openjfx.model.game.Position;
import org.openjfx.service.game.PositionOffsetService;
import org.openjfx.model.game.Player;
import org.openjfx.state.PlayerMoveState;

import java.util.Optional;

public class CaptureMoveValidator implements MoveValidator {

    private final PositionOffsetService positionOffsetService;

    public CaptureMoveValidator() {
        this.positionOffsetService = new PositionOffsetService();
    }

    @Override
    public boolean canMove(Board board, Position position, MoveOffset offsetScalar, Player player) {
        Position positionAfterMove = positionOffsetService.calculate(position, offsetScalar, MoveType.MOVE);
        Position positionAfterCapture = positionOffsetService.calculate(position, offsetScalar, MoveType.CAPTURE);
        return positionAfterCapture.isOnBoard() && canCapture(board, positionAfterMove, positionAfterCapture);
    }

    private boolean canCapture(Board board, Position capturedPiecePosition, Position positionAfterCapture) {
        Optional<Piece> capturedPiece = board.getGrid(capturedPiecePosition).getPiece();
        Optional<Piece> pieceAfterCapture = board.getGrid(positionAfterCapture).getPiece();
        return capturedPiece.isPresent()
                && pieceAfterCapture.isEmpty()
                && isEnemyPiece(capturedPiece.get().getPlayer(), PlayerMoveState.getActualPlayer());
    }

    private boolean isEnemyPiece(Player player1, Player player2) {
        return !player1.equals(player2);
    }
}
