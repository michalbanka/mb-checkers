package org.openjfx.service.game.moves;

import org.openjfx.model.game.Board;
import org.openjfx.gui.game.board.Grid;
import org.openjfx.model.game.MoveOffset;
import org.openjfx.model.game.MoveType;
import org.openjfx.model.game.Position;
import org.openjfx.model.game.ValidPlayerMoveDirections;
import org.openjfx.service.game.PositionOffsetService;
import org.openjfx.model.game.Move;
import org.openjfx.model.game.Player;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PossibleMovesAnalyzer {

    private final PositionOffsetService positionOffsetCalculator;
    private final StandardMoveValidator standardMoveValidator;
    private final CaptureMoveValidator captureMoveValidator;

    public PossibleMovesAnalyzer() {
        this.positionOffsetCalculator = new PositionOffsetService();
        this.standardMoveValidator = new StandardMoveValidator();
        this.captureMoveValidator = new CaptureMoveValidator();
    }

    public List<Move> resolve(Board board, Position actual) {
        return analyzeMoves(board, actual);
    }

    private List<Move> analyzeMoves(Board board, Position position) {
        List<Move> possibleMoves = new ArrayList<>();
        Grid grid = board.getGrid(position);
        if (grid.getPiece().isPresent()) {
            List<MoveOffset> allDirectionsOffsets = ValidPlayerMoveDirections.POSSIBLE_CAPTURE_ALL_DIRECTIONS.getMoveOffsets();
            Player player = grid.getPiece().get().getPlayer();
            switch (player) {
                case WHITE:
                    possibleMoves.addAll(analyzeMovesForDirections(board, position, ValidPlayerMoveDirections.POSSIBLE_WHITE_PIECE_MOVE_DIRECTIONS.getMoveOffsets(), player));
                    possibleMoves.addAll(analyzeCapturesForDirections(board, position, allDirectionsOffsets, player));
                    break;
                case BLACK:
                    possibleMoves.addAll(analyzeMovesForDirections(board, position, ValidPlayerMoveDirections.POSSIBLE_BLACK_PIECE_MOVE_DIRECTIONS.getMoveOffsets(), player));
                    possibleMoves.addAll(analyzeCapturesForDirections(board, position, allDirectionsOffsets, player));
                    break;
                default:
                    throw new InvalidParameterException("Selected piece with unknown player");
            }
        }
        return possibleMoves;
    }

    private List<Move> analyzeMovesForDirections(Board board, Position position, List<MoveOffset> possibleDirections, Player player) {
        return possibleDirections.stream()
                .filter(move -> standardMoveValidator.canMove(board, position, move, player))
                .map(move -> {
                    Position positionAfterMove = positionOffsetCalculator.calculate(position, move, MoveType.MOVE);
                    return new Move(positionAfterMove, Collections.emptyList());
                })
                .collect(Collectors.toList());
    }

    private List<Move> analyzeCapturesForDirections(Board board, Position position, List<MoveOffset> possibleDirections, Player player) {
        return possibleDirections.stream()
                .filter(move -> captureMoveValidator.canMove(board, position, move, player))
                .map(move -> {
                    Position positionAfterCapture = positionOffsetCalculator.calculate(position, move, MoveType.CAPTURE);
                    return new Move(positionAfterCapture,
                            analyzeCapturesForDirections(board, positionAfterCapture, getPossibleDirectionsExcept(move.getOpposedOffset()), player));
                })
                .collect(Collectors.toList());
    }

    private List<MoveOffset> getPossibleDirectionsExcept(MoveOffset move) {
        List<MoveOffset> offsets = ValidPlayerMoveDirections.POSSIBLE_CAPTURE_ALL_DIRECTIONS.getMoveOffsets();
        return offsets.stream()
                .filter(moveOffset -> !moveOffset.equals(move))
                .collect(Collectors.toList());
    }
}
