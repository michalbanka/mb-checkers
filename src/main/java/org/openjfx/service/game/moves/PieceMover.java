package org.openjfx.service.game.moves;

import org.openjfx.model.game.Board;
import org.openjfx.gui.game.board.Grid;
import org.openjfx.gui.game.board.Piece;
import org.openjfx.model.game.Position;
import org.openjfx.model.game.Move;

import java.util.Optional;

public class PieceMover {

    public PieceMover() {}

    public void move(Grid fromGrid, Grid toGrid) {
        Optional<Piece> optionalPiece = fromGrid.getPiece();
        optionalPiece.ifPresent(piece -> {
            fromGrid.removePiece();
            toGrid.addPiece(piece);
        });
    }

    public void remove(Board board, Position basePosition, Move move) {
        if (Math.abs(basePosition.getRow() - move.getPosition().getRow()) >= 2) {
            int row = (move.getPosition().getRow() + basePosition.getRow()) / 2;
            int column = (move.getPosition().getColumn() + basePosition.getColumn()) / 2;
            Position positionToRemove = new Position(column, row);
            board.getGrid(positionToRemove).removePiece();
            move.getNextMoves().forEach(m -> remove(board, move.getPosition(), m));
        }
    }
}
