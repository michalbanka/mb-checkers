package org.openjfx.service.game.moves;

import org.openjfx.model.game.Board;
import org.openjfx.model.game.MoveOffset;
import org.openjfx.model.game.MoveType;
import org.openjfx.model.game.Position;
import org.openjfx.service.game.PositionOffsetService;
import org.openjfx.model.game.Player;

public class StandardMoveValidator implements MoveValidator {

    private final PositionOffsetService positionOffsetService;

    public StandardMoveValidator() {
        this.positionOffsetService = new PositionOffsetService();
    }

    @Override
    public boolean canMove(Board board, Position position, MoveOffset offsetScalar, Player player) {
        Position positionAfterMove = positionOffsetService.calculate(position, offsetScalar, MoveType.MOVE);
        return positionAfterMove.isOnBoard() && !isPiecePresent(board, positionAfterMove);
    }
}
