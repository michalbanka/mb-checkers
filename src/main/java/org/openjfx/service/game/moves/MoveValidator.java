package org.openjfx.service.game.moves;

import org.openjfx.model.game.Board;
import org.openjfx.model.game.MoveOffset;
import org.openjfx.model.game.Position;
import org.openjfx.model.game.Player;

public interface MoveValidator {

    boolean canMove(Board board, Position position, MoveOffset offsetScalar, Player player);

    default boolean isPiecePresent(Board board, Position positionAfterOffset) {
        return board.getGrid(positionAfterOffset).getPiece().isPresent();
    }
}
