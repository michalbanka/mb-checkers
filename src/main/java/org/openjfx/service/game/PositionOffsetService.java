package org.openjfx.service.game;

import org.openjfx.model.game.MoveOffset;
import org.openjfx.model.game.MoveType;
import org.openjfx.model.game.Position;

public class PositionOffsetService {

    public PositionOffsetService() {}

    public Position calculate(Position basePosition, MoveOffset offsetScalar, MoveType moveType) {
        int columnOffset = offsetScalar.getColumnOffset() * moveType.getStepMultiplier();
        int rowOffset = offsetScalar.getRowOffset() * moveType.getStepMultiplier();
        return new Position(basePosition.getColumn() + columnOffset, basePosition.getRow() + rowOffset);
    }
}
